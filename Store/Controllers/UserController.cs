﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Store.API.ApiResultModels;
using Store.API.Auth;
using Store.API.Models;
using Store.API.Services.Contracts;
using Store.DataBase.Entity;
using Store.Repository.Contracts;
using Store.Repository.Implementations;
using System.IdentityModel.Tokens.Jwt;

namespace Store.API.Controllers
{
    /// <summary>
    /// Конроллер пользователей.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// репозиторий пользователей.
        /// </summary>
        private readonly IUserRepository userRepository;

        /// <summary>
        /// Сервис для работы с токеном.
        /// </summary>
        private readonly ITokenService tokenService;

        public UserController(IUserRepository userRepository, ITokenService tokenService)
        {
            this.userRepository = userRepository;
            this.tokenService = tokenService;
        }

        /// <summary>
        /// Регистрация пользователя.
        /// </summary>
        /// <param name="model">Модель регистрации пользователя.</param>
        /// <returns></returns>
        [HttpPost("registration")]
        public async Task<IActionResult> RegistrationUser(RegistrationModel model)
        {
            if (await userRepository.RegistrationUser(new User
            {
                Login = model.Login,
                Password = model.Password,
                Address = model.Address,
                Phone = model.Phone,
                Name = model.Name,
            }))
            {
                return ApiResult.Success();
            }

            return ApiResult.Failed(ErrorCode.UserExists, "Пользователь с данным логином уже зарегистрирован.");
        }

        /// <summary>
        /// Логин пользователя.
        /// </summary>
        /// <param name="model">Логин модель.</param>
        /// <returns>Токен.</returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await userRepository.LoginUser(model.Login, model.Password);
            if (user == null)
            {
                return ApiResult.Failed(ErrorCode.LoginFailed, "Неверный логин или пароль.");
            }

            var jwtToken = tokenService.GenerationToken(user.Id.ToString());

            return ApiResult.Success(new
            {
                token = jwtToken
            });
        }
    }
}

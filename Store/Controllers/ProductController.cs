﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.API.ApiResultModels;
using Store.API.Models;
using Store.DataBase.Entity;
using Store.Repository.Contracts;

namespace Store.API.Controllers
{
    /// <summary>
    /// Контроллер товаров.
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        /// <summary>
        /// Репозиторий товаров.
        /// </summary>
        private readonly IProductRepository productRepository;

        public ProductController(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        /// <summary>
        /// Создание товара.
        /// </summary>
        /// <param name="model">Модель для создания товара.</param>
        /// <returns>Товар.</returns>
        [HttpPost("create")]
        public async Task<IActionResult> CreateProduct(CreateProductModel model)
        {
            var newProduct = new Product
            {
                Title = model.Title,
                Description = model.Description,
                Price = model.Price,
            };
            await productRepository.Add(newProduct);

            return ApiResult.Success(newProduct);
        }

        /// <summary>
        /// Удаление товара.
        /// </summary>
        /// <param name="id">Id товара.</param>
        /// <returns>Товар.</returns>
        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteProduct(Guid id)
        {
            var product = await productRepository.GetById(id);
            if (product == null || product.Deleted)
            {
                return ApiResult.Failed(ErrorCode.ProductNotExists, "Продукт с таким id не найден.");
            }

            product.Deleted = true;
            await productRepository.Update(product);

            return ApiResult.Success(product);
        }

        /// <summary>
        /// Изменение товара.
        /// </summary>
        /// <param name="model">Модель для изменения товара.</param>
        /// <param name="id">Id товара.</param>
        /// <returns>Товар.</returns>
        [HttpPost("edit/{id}")]
        public async Task<IActionResult> EditProduct(CreateProductModel model, Guid id)
        {
            var product = await productRepository.GetById(id);
            if (product == null || product.Deleted)
            {
                return ApiResult.Failed(ErrorCode.ProductNotExists, "Продукт с таким id не найден.");
            }

            product.Title = model.Title;
            product.Description = model.Description;
            product.Price = model.Price;

            await productRepository.Update(product);

            return ApiResult.Success(product);
        }

        /// <summary>
        /// Получить все товары.
        /// </summary>
        /// <returns>Товары.</returns>
        [HttpGet("get_all")]
        public async Task<IActionResult> GetAllProducts()
        {
            return ApiResult.Success(await productRepository.GetAll());
        }
    }
}

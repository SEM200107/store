﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.API.ApiResultModels;
using Store.API.Models;
using Store.API.Services.Contracts;
using Store.DataBase.Entity;
using Store.Repository.Contracts;

namespace Store.API.Controllers
{
    /// <summary>
    /// Контроллер для заказов.
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        /// <summary>
        /// Репозиторий заказов.
        /// </summary>
        private readonly IOrderRepository orderRepository;

        /// <summary>
        /// Репозиторый продуктов.
        /// </summary>
        private readonly IProductRepository productRepository;

        /// <summary>
        /// Репозиторый пользоввателей.
        /// </summary>
        private readonly IUserRepository userRepository;

        /// <summary>
        /// Сервис для работы с токеном.
        /// </summary>
        private readonly ITokenService tokenService;

        public OrderController(IOrderRepository orderRepository, IProductRepository productRepository, IUserRepository userRepository, ITokenService tokenService)
        {
            this.orderRepository = orderRepository;
            this.productRepository = productRepository;
            this.userRepository = userRepository;
            this.tokenService = tokenService;
        }

        /// <summary>
        /// Создание заказа.
        /// </summary>
        /// <param name="model">Модель для создания заказа.</param>
        /// <returns>Заказ.</returns>
        [HttpPost("create")]
        public async Task<IActionResult> CreateOrder(CreateOrderModel model)
        {
            var userId = tokenService.GetUserIdByToken(this.Request.Headers.Authorization.ToString());
            var user = await userRepository.GetById(userId);
            if (user == null)
            {
                return ApiResult.Failed(ErrorCode.UserNotExists, "Пользователь не найден.");
            }

            var notExistsProducts = new List<Guid>();
            var products = new List<Guid>();
            float price = 0;

            foreach(var productId in model.ProductsId)
            {
                var product = await productRepository.GetById(productId);
                if (product != null && !product.Deleted)
                {
                    price += product.Price;
                    products.Add(productId);
                }
                else
                {
                    notExistsProducts.Add(productId);
                }
            }

            if (notExistsProducts.Count == model.ProductsId.Count)
            {
                return ApiResult.Failed(ErrorCode.ProductsNotExists, "Таких продуктов нет для заказа.");
            }

            var newOrder = new Order
            {
                UserId = userId,
                Price = price,
                ProductsId = products
            };

            await orderRepository.Add(newOrder);


            var response = new OrdersResponseModel
            {
                OrderId = newOrder.Id,
                UserName = user.Name,
                UserPhone = user.Phone,
                UserAddress = user.Address,
                Price = price,
                Products = await productRepository.GetByIdList(products)
            };

            if (notExistsProducts.Count > 0)
            {
                var error = "Данных продуктов не существует, они не будут добавлены в заказ: ";
                var errorProducts = string.Join(", ", notExistsProducts);

                return ApiResult.Success(response, ErrorCode.ProductsNotExists, string.Concat(error, errorProducts));
            }

            return ApiResult.Success(response);
        }

        /// <summary>
        /// Получить все заказы пользователя.
        /// </summary>
        /// <returns>Заказы.</returns>
        [HttpGet("get_all")]
        public async Task<IActionResult> GetAllOrder()
        {
            var userId = tokenService.GetUserIdByToken(this.Request.Headers.Authorization.ToString());
            var user = await userRepository.GetById(userId);
            if (user == null)
            {
                return ApiResult.Failed(ErrorCode.UserNotExists, "Пользователь не найден.");
            }

            var orders = await orderRepository.GetOrdersByUserId(userId);

            if (orders.Count == 0)
            {
                return ApiResult.Failed(ErrorCode.NoOrdersByUser, "У данного пользователя нет заказов.");
            }

            var response = new List<OrdersResponseModel>();
            foreach (var order in orders)
            {
                response.Add(new OrdersResponseModel
                {
                    OrderId = order.Id,
                    UserName = order.User.Name,
                    UserPhone = order.User.Phone,
                    UserAddress = order.User.Address,
                    Price = order.Price,
                    Products = await productRepository.GetByIdList(order.ProductsId)
                });
            }

            return ApiResult.Success(response);
        }

        /// <summary>
        /// Удаление заказа.
        /// </summary>
        /// <param name="id">Id заказа.</param>
        /// <returns>Заказ.</returns>
        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteOrderById(Guid id)
        {
            var order = await orderRepository.GetById(id);

            if (order == null)
            {
                return ApiResult.Failed(ErrorCode.OrderNotExists, "Заказ с таким id не найден.");
            }

            var userId = tokenService.GetUserIdByToken(this.Request.Headers.Authorization.ToString());
            var user = await userRepository.GetById(userId);
            if (user == null || order.UserId != userId)
            {
                return ApiResult.Failed(ErrorCode.UserNotExists, "Пользователь не найден.");
            }

            await orderRepository.Remove(order);

            return ApiResult.Success(order);
        }
    }
}

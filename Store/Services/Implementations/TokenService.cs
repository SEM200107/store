﻿using Microsoft.IdentityModel.Tokens;
using Store.API.Auth;
using Store.API.Services.Contracts;
using Store.DataBase.Entity;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Store.API.Services.Implementations
{
    /// <summary>
    /// Сервис токенов.
    /// </summary>
    public class TokenService : ITokenService
    {
        public string? GenerationToken(string userId)
        {
            var claims = new List<Claim>
                {
                    new Claim("userId", userId)
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: claimsIdentity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        public Guid GetUserIdByToken(string token)
        {
            token = token.Substring(7);
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = jsonToken as JwtSecurityToken;

            var userId = tokenS.Claims.First(claim => claim.Type == "userId").Value;

            return Guid.Parse(userId);
        }
    }
}

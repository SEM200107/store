﻿using System.Security.Claims;

namespace Store.API.Services.Contracts
{
    /// <summary>
    /// Интерфейс для сервиса токенов.
    /// </summary>
    public interface ITokenService
    {
        /// <summary>
        /// Сгенерировать токен.
        /// </summary>
        /// <param name="userId">Id пользователя.</param>
        /// <returns>Токен.</returns>
        string? GenerationToken(string userId);

        /// <summary>
        /// Получить id пользователя по токену.
        /// </summary>
        /// <param name="token">Токен.</param>
        /// <returns>Id пользователя.</returns>
        Guid GetUserIdByToken(string token);
    }
}

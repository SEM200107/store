﻿using Store.API.ApiResultModels;

namespace Store.API.Middlewares
{
    /// <summary>
    /// Промежуточное ПО для обработки исключений.
    /// </summary>
    public sealed class ExceptionHandlerMiddleware
    {
        /// <summary>
        /// Следующий в очереди middleware.
        /// </summary>
        private readonly RequestDelegate next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// Асинхронное выполнение middleware.
        /// </summary>
        /// <param name="context">Http-контекст.</param>
        public async Task InvokeAsync(HttpContext context, ILogger<ExceptionHandlerMiddleware> logger)
        {
            try
            {
                await this.next.Invoke(context);
            }
            catch (Exception ex)
            {
                await context.Response.WriteAsJsonAsync(new ApiResult<ApiResult.Empty, ApiError>
                    (null, new ApiError(ErrorCode.InternalServerError, "Нобработанное исключение! Обратитесь к администратору!")));

                var innerException = string.Empty;

                var exception = ex.InnerException;

                while (exception is not null)
                {
                    innerException += $"\n {exception.InnerException}";
                    exception = exception.InnerException;
                }

                logger.LogError($"ErrorMesage - {ex.Message}\nStackTrace - {ex.StackTrace}\nInnerException {innerException}");
            }
        }
    }
}

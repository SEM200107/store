﻿namespace Store.API.Models
{
    /// <summary>
    /// Модель для создания товара.
    /// </summary>
    public class CreateProductModel
    {
        /// <summary>
        /// Заголовок.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public float Price { get; set; }
    }
}

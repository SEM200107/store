﻿using Store.DataBase.Entity;

namespace Store.API.Models
{
    /// <summary>
    /// Модель ответа для заказа.
    /// </summary>
    public class OrdersResponseModel
    {
        /// <summary>
        /// Id заказа.
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// Имя пользователя.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Адрес пользователя.
        /// </summary>
        public string UserAddress { get; set; }

        /// <summary>
        /// Телефон пользователя.
        /// </summary>
        public string UserPhone { get; set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// Товары.
        /// </summary>
        public List<Product> Products {  get; set; } 
    }
}

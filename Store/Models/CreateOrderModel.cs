﻿using Store.DataBase.Entity;

namespace Store.API.Models
{
    /// <summary>
    /// Модель для создания заказа.
    /// </summary>
    public class CreateOrderModel
    {
        /// <summary>
        /// Список id'шников товаров.
        /// </summary>
        public List<Guid> ProductsId { get; set; }
    }
}

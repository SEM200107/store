﻿namespace Store.API.ApiResultModels
{
    /// <summary>
    /// Список ошибок.
    /// </summary>
    public enum ErrorCode
    {
        /// <summary>
        /// Пользователь с данным логином уже зарегистрирован.
        /// </summary>
        UserExists = 1,

        /// <summary>
        /// Неверный логин или пароль.
        /// </summary>
        LoginFailed = 2,

        /// <summary>
        /// Продукт с таким id не найден.
        /// </summary>
        ProductNotExists = 3,

        /// <summary>
        /// Таких продуктов нет для заказа.
        /// </summary>
        ProductsNotExists = 4,

        /// <summary>
        /// У данного пользователя нет заказов.
        /// </summary>
        NoOrdersByUser = 5,

        /// <summary>
        /// Заказ с таким id не найден.
        /// </summary>
        OrderNotExists = 6,

        /// <summary>
        /// Пользователь с таким id не найден.
        /// </summary>
        UserNotExists = 7,

        /// <summary>
        /// Внутренняя ошибка сервера.
        /// </summary>
        InternalServerError = 500
    }
}

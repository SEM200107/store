﻿using Microsoft.AspNetCore.Mvc;

namespace Store.API.ApiResultModels
{
    public sealed class ApiResult
    {
        /// <summary>
        /// Неудачное выполнение метода Web Api.
        /// </summary>
        /// <param name="errorCode">Код ошибки.</param>
        /// <param name="message">Сообщение об ошибке.</param>
        /// <returns>Результат вызова метода Web Api.</returns>
        public static JsonResult Failed(ErrorCode errorCode, string message = "")
        {
            return new JsonResult(ApiResult<Empty, ApiError>.Create(null, new ApiError(errorCode, message)));
        }

        /// <summary>
        /// Неудачное выполнение метода Web Api со списком ошибок.
        /// </summary>
        /// <param name="errorCodes">Список кодов ошибок.</param>
        /// <returns>Результат вызова метода Web Api с набором ошибок.</returns>
        public static JsonResult Failed(params ErrorCode[] errorCodes)
        {
            if (errorCodes == null || !errorCodes.Any())
            {
                throw new ArgumentException(nameof(errorCodes));
            }

            var errorList = errorCodes.Select(code => new ApiError(code, null)).ToArray();

            return new JsonResult(ApiResult<Empty, ApiError[]>.Create(null, errorList));
        }

        /// <summary>
        /// Успешное выполнение метода Web Api.
        /// </summary>
        /// <returns>Результат вызова метода Web Api.</returns>
        public static JsonResult Success()
        {
            return new JsonResult(new ApiResult<Empty, ApiError>(null, null));
        }

        /// <summary>
        /// Успешное выполнение метода Web Api.
        /// </summary>
        /// <param name="data">Данные, полученные в результате вызова метода Web Api.</param>
        /// <returns>Результат вызова метода Web Api.</returns>
        public static JsonResult Success<T>(T? data)
        {
            return new JsonResult(new ApiResult<T, ApiError>(data, null));
        }

        /// <summary>
        /// Успешное выполнение метода Web Api с незначительными ошибками.
        /// </summary>
        /// <param name="data">Данные, полученные в результате вызова метода Web Api.</param>
        /// <param name="errorCode">Код ошибки.</param>
        /// <param name="message">Сообщение ошибки.</param>
        /// <returns>Результат вызова метода Web Api.</returns>
        public static JsonResult Success<T>(T? data, ErrorCode errorCode, string message = "")
        {
            return new JsonResult(new ApiResult<T, ApiError>(data, new ApiError(errorCode, message)));
        }

        #region Nested Types

        /// <summary>
        /// Пустой трансферный объект.
        /// </summary>
        public class Empty
        {
        }

        #endregion
    }

    /// <summary>
    /// Результат вызова метода Web Api.
    /// </summary>
    public sealed class ApiResult<T, TError>
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="data">Данные, полученные в результате вызова метода Web Api.</param>
        /// <param name="error">Ошибка, полученная в результате вызова метода Web Api.</param>
        public ApiResult(T? data, TError? error)
        {
            this.Data = data;
            this.Error = error;
        }

        /// <summary>
        /// Данные, полученные в результате вызова метода Web Api.
        /// </summary>
        public T? Data { get; }

        /// <summary>
        /// Ошибка, полученная в результате вызова метода Web Api.
        /// </summary>
        public TError? Error { get; }

        public static ApiResult<T, TError> Create(T? data, TError? error)
        {
            return new ApiResult<T, TError>(data, error);
        }
    }
}

﻿namespace Store.API.ApiResultModels
{
    /// <summary>
    /// Ошибка, полученная в результате вызова метода Web Api.
    /// </summary>
    public sealed class ApiError
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="code">Код ошибки.</param>
        /// <param name="message">Сообщение об ошибке.</param>
        public ApiError(ErrorCode? code, string? message = "")
        {
            this.Code = code;
            this.Message = message;
        }

        /// <summary>
        /// Код ошибки.
        /// </summary>
        public ErrorCode? Code { get; }

        /// <summary>
        /// Сообщение об ошибке.
        /// </summary>
        public string? Message { get; }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Store.Repository.Contracts;
using Store.Repository.Implementations;

namespace Store.Repository
{
    /// <summary>
    /// Вспомогательный класс для реализации метода инъекции сервисов для работы с репозиториями.
    /// </summary>
    public static class RepositoryInjector
    {
        /// <summary>
        /// Добавить сервисы для работы с репозиториями.
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        public static void AddRepository(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
        }
    }
}
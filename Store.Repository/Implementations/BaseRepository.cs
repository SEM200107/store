﻿using Microsoft.EntityFrameworkCore;
using Store.DataBase;
using Store.DataBase.Entity;
using Store.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Repository.Implementations
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        public readonly DatabaseContext DbContext;

        public BaseRepository()
        {
            DbContext = new DatabaseContext(DbConfiguration.Configuration["ConnectionString"]);
        }

        public async Task<T> GetById(Guid id)
        {
            return await DbContext.Set<T>().FindAsync(id);
        }

        public async Task<List<T>> GetByIdList(List<Guid> ids)
        {
            return await DbContext.Set<T>()
                .Where(entity => ids.Contains(entity.Id)).OrderBy(e => e.Id)
                .ToListAsync();
        }

        public async Task<List<T>> GetAll()
        {
            return await DbContext.Set<T>().OrderBy(e => e.Id).ToListAsync();
        }

        public async Task Add(T entity)
        {
            await DbContext.Set<T>().AddAsync(entity);
            await DbContext.SaveChangesAsync();
        }

        public async Task AddRange(IEnumerable<T> entities)
        {
            await DbContext.Set<T>().AddRangeAsync(entities);
            await DbContext.SaveChangesAsync();
        }

        public async Task Remove(T entity)
        {
            DbContext.Set<T>().Remove(entity);
            await DbContext.SaveChangesAsync();
        }

        public async Task RemoveRange(IEnumerable<T> entities)
        {
            DbContext.Set<T>().RemoveRange(entities);
            await DbContext.SaveChangesAsync();
        }

        public async Task Update(T entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
            await DbContext.SaveChangesAsync();
        }
    }
}

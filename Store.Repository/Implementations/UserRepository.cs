﻿using Microsoft.EntityFrameworkCore;
using Store.DataBase.Entity;
using Store.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Repository.Implementations
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public async Task<User> LoginUser(string login, string password)
        {
            var user = await DbContext.Users.FirstOrDefaultAsync(u => u.Login == login);
            if (user == null || !HashPass.VerifyHashedPassword(user.Password, password))
            {
                return null;
            }

            return user;
        }

        public async Task<bool> RegistrationUser(User user)
        {
            var userInDb = await DbContext.Users.FirstOrDefaultAsync(u => u.Login == user.Login);
            if (userInDb != null)
            {
                return false;
            }

            user.Password = HashPass.HashPassword(user.Password);
            await DbContext.Users.AddAsync(user);
            await DbContext.SaveChangesAsync();

            return true;
        }
    }
}

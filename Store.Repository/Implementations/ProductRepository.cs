﻿using Microsoft.EntityFrameworkCore;
using Store.DataBase.Entity;
using Store.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Repository.Implementations
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public async Task<List<Product>> GetAll()
        {
            return await DbContext.Products.Where(p => !p.Deleted).OrderBy(p => p.Id).ToListAsync();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Store.DataBase.Entity;
using Store.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Repository.Implementations
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public async Task<List<Order>> GetOrdersByUserId(Guid userId)
        {
            return await DbContext.Orders.Where(o => o.UserId == userId).OrderBy(o => o.Id).ToListAsync();
        }
    }
}

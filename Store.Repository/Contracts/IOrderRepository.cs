﻿using Store.DataBase.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Repository.Contracts
{
    /// <summary>
    /// Интерфейс репозитория заказов.
    /// </summary>
    public interface IOrderRepository : IBaseRepository<Order>
    {
        /// <summary>
        /// Получить заказы пользователя.
        /// </summary>
        /// <param name="userId">Пользователь.</param>
        /// <returns>Лист с заказами.</returns>
        Task<List<Order>> GetOrdersByUserId(Guid userId);
    }
}

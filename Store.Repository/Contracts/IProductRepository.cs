﻿using Store.DataBase.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Repository.Contracts
{
    /// <summary>
    /// Интерфейс репозитория товаров.
    /// </summary>
    public interface IProductRepository : IBaseRepository<Product>
    {
    }
}

﻿using Store.DataBase.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Repository.Contracts
{
    /// <summary>
    /// Интерфейс репозитория пользователя.
    /// </summary>
    public interface IUserRepository : IBaseRepository<User>
    {
        /// <summary>
        /// Регистрация пользователя.
        /// </summary>
        /// <param name="model">Модель юзера.</param>
        /// <returns>Удалось зарегистрировать или нет.</returns>
        Task<bool> RegistrationUser(User model);

        /// <summary>
        /// Залогинить пользователя.
        /// </summary>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>При успешном логине модель юзера, иначе null</returns>
        Task<User> LoginUser(string login, string password);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.DataBase.Entity
{
    /// <summary>
    /// Продукт.
    /// </summary>
    public class Product : BaseEntity
    {
        /// <summary>
        /// Заголовок.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// Флаг, удаленный ли товар.
        /// </summary>
        public bool Deleted { get; set; } = false;
    }
}

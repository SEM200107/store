﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Store.DataBase.Entity
{
    /// <summary>
    /// Базовая модель для сущностей.
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        
        /// <summary>
        /// Дата создания сущности.
        /// </summary>
        public DateTime DateOfCreation { get; set; } = DateTime.UtcNow;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.DataBase.Entity
{
    /// <summary>
    /// Заказ.
    /// </summary>
    public class Order : BaseEntity
    {
        /// <summary>
        /// Пользователь.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Id пользователя.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// Список id'шников продуктов в заказе.
        /// </summary>
        public List<Guid> ProductsId { get; set; }
    }
}

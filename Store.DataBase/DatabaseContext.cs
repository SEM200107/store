﻿using Microsoft.EntityFrameworkCore;
using Store.DataBase.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.DataBase
{
    /// <summary>
    /// Контекст БД.
    /// </summary>
    public class DatabaseContext : Microsoft.EntityFrameworkCore.DbContext
    {
        /// <summary>
        /// Пользователи.
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Товары.
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Заказы.
        /// </summary>
        public DbSet<Order> Orders { get; set; }
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DatabaseContext(string connectionString)
            : base(GetOptions(connectionString))
        {
            Database.EnsureCreated();
        }

        private static DbContextOptions GetOptions(string connectionString)
        {
            var options = new DbContextOptionsBuilder();
            options.UseLazyLoadingProxies();
            return NpgsqlDbContextOptionsBuilderExtensions.UseNpgsql(options, connectionString).Options;
        }
    }
}
